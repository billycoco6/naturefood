import React from 'react'
import { Font } from 'expo';
import RootStack from './route'

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fontLoaded: false
    }
  }

  async componentWillMount() {
    await Font.loadAsync({
      'thai-sans-bold': require('./src/assets/fonts/ThaiSansNeue-Bold.otf'),
      'thai-sans': require('./src/assets/fonts/ThaiSansNeue-Regular.ttf'),
    })
    this.setState({ fontLoaded: true })
  }

  render() {
    if (this.state.fontLoaded) {
      return (
        <RootStack />
      );
    } else {
      return (
        <Expo.AppLoading />
      )
    }
  }
}
