import React from 'react'
import {
    createSwitchNavigator,
    createStackNavigator,
    createDrawerNavigator
} from 'react-navigation'
import {
    MaterialIcons
} from '@expo/vector-icons'
import AuthLoadingScreen from './src/screen/authLoading'
import WelcomeScreen from './src/screen/welcome'
import SigninScreen from './src/screen/auth'
import RegisterScreen from './src/screen/register'
import StoreScreen from './src/screen/stores/store'
import CartScreen from './src/screen/stores/cart'
import ProfileScreen from './src/screen/profile/profile'
import PaymentScreen from './src/screen/stores/payment'
import HistoryScreen from './src/screen/history/history'
import SubscriptionHistoryScreen from './src/screen/history/subscription'
import CreditCardScreen from './src/screen/stores/creditcard'
import PackagePlanScreen from './src/screen/stores/packagePlan'
import MyWalletChargeScreen from './src/screen/stores/myWalletCharge'

export const AuthStack = createStackNavigator(
    {
        Welcome: WelcomeScreen,
        Signin: SigninScreen,
        Register: RegisterScreen,
    },
    {
        initialRouteName: 'Welcome'
    }
)

const StoreScreenStack = createStackNavigator(
    {
        Store: StoreScreen,
        Cart: CartScreen,
        Payment: PaymentScreen,
        CreditCard: CreditCardScreen,
        PackagePlan: PackagePlanScreen,
        MyWalletCharge: MyWalletChargeScreen,
    },
    {
        initialRouteName: 'Store'
    }
)

const ProfileScreenStack = createStackNavigator(
    {
        Profile: ProfileScreen,
    }
)

const HistoryScreenStack = createStackNavigator(
    {
        History: HistoryScreen,
    }
)

const SubscriptionHistoryStack = createStackNavigator(
    {
        SubscriptionHistory: SubscriptionHistoryScreen,
    }
)

export const AppDrawer = createDrawerNavigator(
    {
        StoreStack: {
            screen: StoreScreenStack,
            navigationOptions: {
                drawerLabel: 'ร้านค้า',
                drawerIcon: ({tintColor}) => (
                    <MaterialIcons name={'store'} size={28} color={tintColor}/>
                ),
                drawerLockMode: 'locked-closed',
            },
        },
        ProfileStack: {
            screen: ProfileScreenStack,
            navigationOptions: {
                drawerLabel: 'โปรไฟล์',
                drawerIcon: ({tintColor}) => (
                    <MaterialIcons name={'person'} size={28} color={tintColor}/>
                ),
                drawerLockMode: 'locked-closed',
            },
        },
        SubscriptionHistoryStack: {
            screen: SubscriptionHistoryStack,
            navigationOptions: {
                drawerLabel: 'รายการแพ็คเกจ',
                drawerIcon: ({tintColor}) => (
                    <MaterialIcons name={'class'} size={28} color={tintColor}/>
                ),
                drawerLockMode: 'locked-closed',
            },
        },
        HistoryStack: {
            screen: HistoryScreenStack,
            navigationOptions: {
                drawerLabel: 'รายการสั่งซื้อ',
                drawerIcon: ({tintColor}) => (
                    <MaterialIcons name={'history'} size={28} color={tintColor}/>
                ),
                drawerLockMode: 'locked-closed',
            },
        },
    }
)

// this we will create check auth logic later
export default createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        Auth: AuthStack,
        App: AppDrawer,
    },
    {
        initialRouteName: 'AuthLoading',
    }
)
