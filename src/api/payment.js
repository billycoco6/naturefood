import {
  AsyncStorage,
} from 'react-native'
import axios from 'axios'
import {URL} from './constants'

const uploadSlip = async ({slip}) => {
  const token = await AsyncStorage.getItem('token')
  const data = {
    data: slip,
  }
return axios.post(`${URL}/uploadslip`,
  data,
  {
    headers: { Authorization: `Bearer ${token}` }
  }
  ).then((res) => (res)
  ).catch((err) => (err))
}

const getCardToken = async ({cart, isPackage, duration, deliverWeek, number, expirationMonth, expirationYear, securityCode, name}) => {
  console.log('GET CARD TOKEN')
  const order = cart.map(({_id, amount}) => ({_id, amount}))
  const data = {
    rememberCard: false,
    card: {
      number,
      expirationMonth,
      expirationYear,
      securityCode,
      name,
    }
  }
  return axios.post('https://api.gbprimepay.com/v1/tokens', 
  data,
  {
    headers: { Authorization: 'Basic Y3hQOXk4cEVNa21BcEJ3OHRrd1drWGVtcVpUeWVySmY6' }
  }
  ).then((res) => (res)
  ).catch((err) => (err))
}

// id represents either subscription id or order id
const cardCharge = async ({cardInfo, id, isSubscription}) => {
  console.log('Card Charge API')
  const token = await AsyncStorage.getItem('token')
  const card = cardInfo.cardInfo.data
  console.log({card})
  console.log({id})
  if (isSubscription) {
    console.log('Subscription and request to sever')
    return axios.post(`${URL}/creditcardcharge`,
      { cardinfo: card, subscription: id },
      {
        headers: { Authorization: `Bearer ${token}` }
      }
      ).then((res) => (res)
      ).catch((err) => (err))
  } else {
    console.log('Order and request to server')
    return axios.post(`${URL}/creditcardcharge`,
      { cardinfo: card, order: id },
      {
        headers: { Authorization: `Bearer ${token}` }
      }
      ).then((res) => (res)
      ).catch((err) => (err))
  }
}

const walletCharge = async ({id, isSubscription}) => {
  console.log('Wallet Charge API')
  const token = await AsyncStorage.getItem('token')
  if (isSubscription) {
    console.log('Subscription and request to server')
    return axios.post(`${URL}/walletcharge`,
      { subscription: id },
      {
        headers: { Authorization: `Bearer ${token}` }
      }
      ).then((res) => (res))
  } else {
    console.log('Order and request to server')
    return axios.post(`${URL}/walletcharge`,
      { order: id },
      {
        headers: { Authorization: `Bearer ${token}` }
      }
      ).then((res) => (res))
  }
}

export {
  uploadSlip,
  getCardToken,
  walletCharge,
  cardCharge,
}