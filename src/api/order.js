import {
    AsyncStorage
} from 'react-native'
import axios from 'axios'
import _ from 'lodash'
import {URL} from './constants'
import {cardCharge, walletCharge} from './payment'

const getOrder = async () => {
    const token = await AsyncStorage.getItem('token')
    return axios.get(`${URL}/getorder`, {
        headers: {'Authorization': `Bearer ${token}`}
    })
    .then((res) => (res))
}

const createOrder = async ({cart, cardInfo}) => {
    const isSubscription = false
    const token = await AsyncStorage.getItem('token')
    const items = cart.map(({_id, amount}) => ({_id, amount}))
    const resCreateOrder = await axios.post(`${URL}/createorder`,
        { items },
        {
            headers: { 'Authorization': `Bearer ${token}` }
        })

    if (resCreateOrder.status === 200) {
        const id = resCreateOrder.data._id
        if (_.isNil(cardInfo)) {
            console.log('this is wallet')
            // charge wallet
            return walletCharge({id, isSubscription})
        } else {
            console.log('this is card')
            // charge card
            return cardCharge({cardInfo, id, isSubscription})
        }
    }
}


export {
    getOrder,
    createOrder,
}