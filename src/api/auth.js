import {AsyncStorage} from 'react-native'
import axios from 'axios'
import {URL} from './constants'

const hello = ({token}) => {
  return axios.get(`${URL}/`, {
    headers: {'Authorization': `Bearer ${token}`}
  })
    .then((res) => (res))
    .catch((err) => (err))
}

const signup = ({email, password, name, address, tel}) => {
  console.log('signup api')
  console.log(email, password, name, address, tel)
  return axios.post(`${URL}/signup`, {email, password, name, address, tel})
}

const signin = ({email, password}) => {
  return axios.post(`${URL}/signin`, {email, password})
}

const checkSignin = async () => {
  const token = await AsyncStorage.getItem('token')
  return token != undefined
}

export {
  signup,
  signin,
  hello,
  checkSignin,
}
