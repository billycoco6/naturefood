import {
    AsyncStorage
} from 'react-native'
import axios from 'axios'
import _ from 'lodash'
import {URL} from './constants'
import { walletCharge, cardCharge } from './payment'

const getSubscription = async () => {
    const token = await AsyncStorage.getItem('token')
    return axios.get(`${URL}/getsubscription`, {
        headers: {'Authorization': `Bearer ${token}`}
    })
    .then((res) => (res))
}

const createSubscription = async ({cart, deliverWeek, duration, cardInfo}) => {
    console.log('Create subscription function')
    const isSubscription = true
    const token = await AsyncStorage.getItem('token')
    const items = cart.map(({_id, amount}) => ({_id, amount}))
    // console.log({cardInfo})

    const resCreateSubscription = await axios.post(`${URL}/createsubscription`,
        { items, deliverweek: deliverWeek, duration },
        {
            headers: { 'Authorization': `Bearer ${token}` }
    })

    console.log({resCreateSubscription})
    if (resCreateSubscription.status === 200) {
        const id = resCreateSubscription.data._id
        if (_.isNil(cardInfo)) {
            console.log('this is wallet')
            return walletCharge({ id, isSubscription })
        } else {
            console.log('this is card')
            return cardCharge({ cardInfo, id, isSubscription })
        }
    }
}


export {
    getSubscription,
    createSubscription,
}