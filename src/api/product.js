import axios from 'axios'
import { AsyncStorage } from 'react-native'
import {URL} from './constants'

const getProduct = async () => {
    const token = await AsyncStorage.getItem('token')
    console.log({token})
    return axios.get(`${URL}/getproduct`, {
      headers: {'Authorization': `Bearer ${token}`}
    })
    .then((res) => res.data)
}

const getPackageProduct = async () => {
    const token = await AsyncStorage.getItem('token')
    return axios.get(`${URL}/getpackageproduct`, {
      headers: {'Authorization': `Bearer ${token}`}
    })
    .then((res) => {
        return res.data
    })
}

export {
    getProduct,
    getPackageProduct,
}