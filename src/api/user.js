import {
    AsyncStorage,
  } from 'react-native'
  import axios from 'axios'
  import {URL} from './constants'

  const getProfile = async () => {
    const token = await AsyncStorage.getItem('token')

    return axios.get(`${URL}/currentuser`,
    {
        headers: { Authorization: `Bearer ${token}` }
    }
    ).then((res) => res.data)
  }

  const editProfile = async ({name, address, tel}) => {
    const token = await AsyncStorage.getItem('token')
    const data = {name, address, tel}
    console.log(data)

    return axios.post(`${URL}/edituser`,
    data,
    {
        headers: { Authorization: `Bearer ${token}` }
    }
    ).then((res) => (res)
    ).catch((err) => (err))
  }



  export {
    getProfile,
    editProfile,
  }
