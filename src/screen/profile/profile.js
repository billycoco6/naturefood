import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  TextInput,
  StatusBar,
  Button,
  AsyncStorage,
  Alert,
} from 'react-native'
import {
  MaterialIcons
} from '@expo/vector-icons'
import Modal from 'react-native-modal'
import Spinner from 'react-native-loading-spinner-overlay';
import {StackActions, NavigationActions} from 'react-navigation';
import {getProfile, editProfile} from '../../api/user'
import {uploadSlip} from '../../api/payment'

const {width, height} = Dimensions.get('window')

export default class ProfileScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      isModalVisible: false,
      name: '',
      email: '',
      address: '',
      tel: '',
      wallet: 0,
      editName: '',
      editAddress: '',
      editTel: '',
    }
  }

  static navigationOptions = ({navigation}) => ({
    headerMode: 'screen',
    headerTitle: 'โปรไฟล์',
    headerLeft: (
      <TouchableOpacity style={{marginLeft: 5}} onPress={() => navigation.openDrawer()}>
        <MaterialIcons name={'list'} size={30} color={'#fff'}/>
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: '#03ac13',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      color: '#fff'
    }
  })

  componentDidMount = async () => {
    const profile = await getProfile()
    console.log(profile)
    this.setState({
      name: profile.name,
      email: profile.email,
      address: profile.address,
      wallet: profile.wallet,
      tel: profile.tel,
    })
  }

  _signout = async () => {
    await AsyncStorage.clear()
    this.props.navigation.navigate('Auth')
  }

  _loadAccountInfo = () => {
    return (
      <View style={[styles.accountView, {paddingVertical: 15}]}>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.header}>{this.state.name}</Text>
          <TouchableOpacity onPress={() => {
            this.setState({isModalVisible: true})
            this.setState({editName: this.state.name})
            this.setState({editAddress: this.state.address})
            this.setState({editTel: this.state.tel})
          }}><MaterialIcons style={{marginLeft: 5, marginTop: 4}} name={'edit'} size={20}
                            color={'#868686'}/></TouchableOpacity>
        </View>
        <Text style={[styles.email, styles.thText, {marginTop: height * 0.015}]}>{this.state.email}</Text>
        <Text style={[styles.email, styles.thText, {marginTop: height * 0.015}]}>{this.state.tel}</Text>
        <Text numberOfLines={2}
              style={[styles.email, styles.thText, {margin: height * 0.015}]}>{this.state.address}</Text>
      </View>
    )
  }

  _cameraRollPermissions = async () => {
    const {Permissions} = Expo
    const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (status !== 'granted') {
      alert('Camera Roll permission is required for you to upload the evidence of payment for us to assess your wallet recharge')
    }
  }

  _uploadSlip = async () => {
    await this._cameraRollPermissions()
    const options = {
      allowsEditing: false,
      quality: 0.5,
      base64: true,
    }

    const response = await Expo.ImagePicker.launchImageLibraryAsync(options)
    if (!response.cancelled) {
      this.setState({loading: true})
      const res = await uploadSlip({
        slip: response.base64,
      })

      if (res.status === 200) {
        Alert.alert(
          'อัพโหลดรูปสำเร็จ',
          '',
          [
            {text: 'ตกลง', onPress: () => this.setState({loading: false})}
          ],
          {cancelable: false},
        )
      } else if (res.status > 200) {
        Alert.alert(
          'ไม่สามารถอัพโหลดรูปได้',
          'กรุณาตรวจสอบขนาดรูปและลองใหม่อีกครั้ง',
          [
            {text: 'ตกลง', onPress: () => this.setState({loading: false})}
          ],
          {cancelable: false},
        )
      }
    }
  }

  _balance = () => {
    return (
      <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 15}}>
        <Text style={styles.balance}>฿ {this.state.wallet}</Text>
        <TouchableOpacity onPress={() => this._uploadSlip()} style={{
          borderWidth: 1,
          borderColor: '#8ABB35',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#8ABB35',
          borderRadius: 5
        }}>
          <Text style={[styles.buttonText, styles.thText, {margin: 5}]}>อัพโหลดหลักฐานโอนเงิน</Text>
        </TouchableOpacity>
      </View>
    )
  }

  _loadEwallet = () => {
    return (
      <View style={[styles.accountView, {height: height * 0.3}]}>
        <View
          style={[styles.subView, {flex: 2, borderBottomWidth: 1, borderBottomColor: '#888888', width: width * 0.8}]}>
          <Text style={[styles.header, styles.thText]}>My Wallet</Text>
        </View>
        <View style={{flex: 3, width: width * 0.8, justifyContent: 'center'}}>
          <Text style={[styles.email, styles.thText, {fontSize: 20}]}>จำนวนเงินใน e-wallet</Text>
          {this._balance()}
        </View>
      </View>
    )
  }

  _loadSignOut = () => {
    return (
      <TouchableOpacity onPress={() => this._signout()}
                        style={[styles.accountView, {height: height * 0.08, backgroundColor: '#989898'}]}>
        <Text style={[styles.buttonText, {fontSize: 20, fontFamily: 'thai-sans-bold'}]}>ออกจากระบบ</Text>
      </TouchableOpacity>
    )
  }

  _editProfileModal = () => {
    return (
      <Modal isVisible={this.state.isModalVisible}>
        <View style={{
          backgroundColor: '#fff',
          margin: 20,
          borderRadius: 10,
          padding: 20,
          marginVertical: height * 0.27,
        }}>
          <Text style={[styles.header, styles.thText]}>แก้ไขข้อมูลส่วนตัว</Text>
          <View>
            <TextInput style={[styles.textInputStyle, {marginTop: height * 0.03}]}
                       value={this.state.editName}
                       onChangeText={(editName) => this.setState({editName})}
                       placeholder={"ชื่อ-นามสกุล"}
                       placeholderTextColor="#cdcdcd"
                       autoCapitalize="none"
                       autoCorrect={false}
                       underlineColorAndroid="#fff"
            />
            <TextInput style={[styles.textInputStyle, {marginTop: height * 0.03}]}
                       value={this.state.editAddress}
                       onChangeText={(editAddress) => this.setState({editAddress})}
                       placeholder={"ที่อยู่"}
                       placeholderTextColor="#cdcdcd"
                       autoCapitalize="none"
                       autoCorrect={false}
                       underlineColorAndroid="#fff"
            />
            <TextInput style={[styles.textInputStyle, {marginTop: height * 0.03}]}
                       value={this.state.editTel}
                       onChangeText={(editTel) => this.setState({editTel})}
                       placeholder={"เบอร์โทรศัพท์"}
                       placeholderTextColor="#cdcdcd"
                       autoCapitalize="none"
                       autoCorrect={false}
                       underlineColorAndroid="#fff"
                       keyboardType={"numeric"}
            />
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 10}}>
            <Button
              onPress={() => this.setState({isModalVisible: !this.state.isModalVisible})}
              title="ยกเลิก"
              color="#6A6A6A"
            />
            <Button
              onPress={async () => {
                const res = await editProfile({name: this.state.editName, address: this.state.editAddress, tel: this.state.editTel})
                await this.setState({
                  name: res.data.name,
                  address: res.data.address,
                  tel: res.data.tel,
                })
                this.setState({isModalVisible: !this.state.isModalVisible})
              }
              }
              title="ยืนยัน"
              color="#8ABB35"
            />
          </View>
        </View>
      </Modal>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
        />
        <Spinner
          visible={this.state.loading}
          textContent={'Uploading photo...'}
          textStyle={{fontSize: 14, color: '#fff'}}
        />
        {this._loadAccountInfo()}
        {this._loadEwallet()}
        {this._loadSignOut()}
        {this._editProfileModal()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#efefef',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  accountView: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    width: width * 0.9,
    marginTop: height * 0.02,
  },
  subView: {
    justifyContent: 'center',
  },
  header: {
    fontSize: 28,
    color: '#454545',
  },
  email: {
    fontSize: 20,
    color: '#868686',
  },
  balance: {
    fontWeight: '300',
    fontSize: 30,
    color: '#676767',
  },
  buttonText: {
    fontSize: 18,
    color: '#fff',
  },
  thText: {
    fontFamily: 'thai-sans',
  },
  textInputStyle: {
    height: height * 0.06,
    borderColor: '#9A9A9A',
    borderWidth: 1,
    borderRadius: 4,
    paddingLeft: 15,
    color: '#616161',
    fontFamily: 'thai-sans',
    fontSize: 18,
  },
})
