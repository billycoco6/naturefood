import React from 'react'
import {
  StyleSheet,
  Text,
  ScrollView,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  StatusBar,
} from 'react-native'
import {
  MaterialIcons
} from '@expo/vector-icons'
import { Card, Button, SearchBar } from 'react-native-elements'
import { getSubscription } from '../../api/subscription'

const { width, height } = Dimensions.get('window')

export default class SubscriptionHistoryScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      subscriptions: []
    }
  }

  static navigationOptions = ({navigation}) =>({
    headerMode: 'screen',
    headerTitle: 'รายการแพ็คเกจ',
    headerLeft: (
      <TouchableOpacity style={{marginLeft: 5}} onPress={() => navigation.openDrawer()}>
        <MaterialIcons name={'list'} size={30} color={'#fff'} />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: '#03ac13',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      color: '#fff'
    }
  })

  componentDidMount = async () => {
    const res = await getSubscription()
    if (res.status === 200) {
        console.log('Component Did Mount')
        const subscriptions = res.data
        this.setState({ subscriptions })
    }
  }

  _numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  _loadHistory = () => {
      console.log('HELLO')
      console.log(this.state.subscriptions)
    if (this.state.subscriptions.length > 0) {
        return (
            <View>
                {
                this.state.subscriptions.map((subscription) => (
                    <Card key={subscription._id} titleStyle={{fontSize: 24}} fontFamily={'thai-sans-bold'} title={'ระยะเวลาแพ็คเกจถึง: ' + subscription.end_date.substring(0,10)}>
                    <View style={{flexDirection: 'row', marginBottom: 15}}>
                        <Text style={[styles.headerText, {flex: 4}]}>รายการ</Text>
                        <Text style={[styles.headerText, {textAlign: 'right', flex: 1}]}>จำนวน</Text>
                        <Text style={[styles.headerText, {textAlign: 'right', flex: 1}]}>ราคา</Text>
                    </View>
                    <View style={{marginBottom: 15}}>
                        {
                        subscription.items.map((item) => (
                            <View key={item._id} style={{flexDirection: 'row'}}>
                            <Text style={[styles.itemText, {flex: 4}]}>{item.name}</Text>
                            <Text style={[styles.itemText, {textAlign: 'right', flex: 1}]}>{item.amount}</Text>
                            <Text style={[styles.itemText, {textAlign: 'right', flex: 1}]}>{this._numberWithCommas(item.price)}</Text>
                            </View>
                        ))
                        }
                    </View>
                    <View style={{borderTopWidth: 1, borderTopColor: '#e1e1e1', paddingTop: 15}}>
                        <Text style={[styles.summaryText, {color: '#8ABB35'}]}>รวม {this._numberWithCommas(subscription.total_price)} บาท</Text>
                        <Text style={[styles.summaryText, {color: '#545454'}]}>จัดส่งทุกสัปดาห์ที่ {subscription.deliverweek} ของเดือน</Text>
                    </View>
                    </Card>
                ))
                }
            </View>
        )
    }
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <StatusBar
          barStyle="light-content"
        />
        { this._loadHistory() }
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#efefef',
  },
  headerText: {
    flex: 1,
    fontSize: 22,
    color: '#343434',
    fontFamily: 'thai-sans-bold',
  },
  itemText: {
    flex: 1,
    fontSize: 20,
    color: '#545454',
    fontFamily: 'thai-sans',
  },
  summaryText: {
    fontSize: 22,
    fontFamily: 'thai-sans-bold',
  },
})
