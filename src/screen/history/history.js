import React from 'react'
import {
  StyleSheet,
  Text,
  ScrollView,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  StatusBar,
} from 'react-native'
import {
  MaterialIcons
} from '@expo/vector-icons'
import { Card, Button, SearchBar } from 'react-native-elements'
import { getOrder } from '../../api/order'
import IconBadge from 'react-native-icon-badge'
import { NavigationEvents } from 'react-navigation'

const { width, height } = Dimensions.get('window')

export default class HistoryScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      orders: []
    }
  }

  static navigationOptions = ({navigation}) =>({
    headerMode: 'screen',
    headerTitle: 'รายการสั่งซื้อ',
    headerLeft: (
      <TouchableOpacity style={{marginLeft: 5}} onPress={() => navigation.openDrawer()}>
        <MaterialIcons name={'list'} size={30} color={'#fff'} />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: '#03ac13',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      color: '#fff'
    }
  })

  componentDidMount = async () => {
    console.log('GET ORDER')
    const res = await getOrder()
    if (res.status === 200) {
      const orders = res.data
      this.setState({ orders })
      console.log(this.state.orders)
    }
  }

  _numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  _loadHistory = () => {
    return (
      <View>
        {
          this.state.orders.map((order) => (
            <Card key={order._id} titleStyle={{fontSize: 24}} fontFamily={'thai-sans-bold'} title={'วันที่สั่งซื้อ: ' + order.created_time.substring(0,10)}>
              <View style={{flexDirection: 'row', marginBottom: 15}}>
                <Text style={[styles.headerText, {flex: 4}]}>รายการ</Text>
                <Text style={[styles.headerText, {textAlign: 'right', flex: 1}]}>จำนวน</Text>
                <Text style={[styles.headerText, {textAlign: 'right', flex: 1}]}>ราคา</Text>
              </View>
              <View style={{marginBottom: 15}}>
                {
                  order.items.map((item) => (
                    <View key={item._id} style={{flexDirection: 'row'}}>
                      <Text style={[styles.itemText, {flex: 4}]}>{item.name}</Text>
                      <Text style={[styles.itemText, {textAlign: 'right', flex: 1}]}>{item.amount}</Text>
                      <Text style={[styles.itemText, {textAlign: 'right', flex: 1}]}>{this._numberWithCommas(item.price)}</Text>
                    </View>
                  ))
                }
              </View>
              <View style={{borderTopWidth: 1, borderTopColor: '#e1e1e1', paddingTop: 15}}>
                <Text style={styles.price}>รวม {this._numberWithCommas(order.total_price)} บาท</Text>
              </View>
            </Card>
          ))
        }
      </View>
    )
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <StatusBar
          barStyle="light-content"
        />
        { this._loadHistory() }
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#efefef',
  },
  headerText: {
    flex: 1,
    fontSize: 22,
    color: '#343434',
    fontFamily: 'thai-sans-bold',
  },
  itemText: {
    flex: 1,
    fontSize: 20,
    color: '#545454',
    fontFamily: 'thai-sans',
  },
  price: {
    fontSize: 22,
    color: '#8ABB35',
    fontFamily: 'thai-sans-bold',
  },
})
