import React from 'react'
import {
  StatusBar,
  StyleSheet,
  Text,
  Dimensions,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  AsyncStorage,
  Alert,
  KeyboardAvoidingView,
} from 'react-native'
import Button from 'apsl-react-native-button'
import {Ionicons} from '@expo/vector-icons';
import {signin} from '../api/auth'

const {width, height} = Dimensions.get('window')

export default class AuthScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }

  static navigationOptions = ({navigation}) => {
    return {
      title: 'เข้าสู่ระบบ',
      headerStyle: {
        backgroundColor: '#03ac13',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        color: '#fff',
        fontFamily: 'thai-sans',
        fontSize: 24
      },
      headerLeft: <TouchableOpacity style={{marginLeft: 10}} onPress={() => navigation.navigate('Store')}><Ionicons
        name="ios-home" size={26} color="white"/></TouchableOpacity>,
    }
  }

  _signin = async () => {
    try {
      const res = await signin(
        {
          email: this.state.email,
          password: this.state.password
        })
      console.log({res})
      switch (res.status) {
        case 200:
          await AsyncStorage.setItem('token', res.data)
          this.props.navigation.navigate('App')
          break
      }
    } catch (e) {
      switch (e.response.status) {
        case 401:
          Alert.alert(
            'อีเมล์/รหัสผ่าน ไม่ถูกต้อง',
            '',
            [{text: 'OK'}]
          )
          break
      }
    }
  }

  _signup = () => {
    this.props.navigation.navigate('Register')
  };

  _loadLogoContainer = () => {
    return (
      <View style={styles.logoContainer}>

      </View>
    )
  };

  _loadFormContainer = () => {
    return (
      <View style={styles.formContainer}>
        <Image style={styles.logoStyle}
               resizeMode="contain"
               source={require('../assets/logo.png')}/>
        <View style={{margin: 10}}>
          <TextInput style={styles.textInputStyle}
                     value={this.state.email}
                     onChangeText={(email) => this.setState({email})}
                     placeholder={"อีเมล์"}
                     placeholderTextColor="#cdcdcd"
                     autoCapitalize="none"
                     autoCorrect={false}
                     underlineColorAndroid="#fff"
          />
        </View>
        <View style={{margin: 10}}>
          <TextInput style={styles.textInputStyle}
                     value={this.state.password}
                     onChangeText={(password) => this.setState({password})}
                     placeholder={"รหัสผ่าน"}
                     secureTextEntry={true}
                     placeholderTextColor="#cdcdcd"
                     autoCapitalize="none"
                     autoCorrect={false}
                     underlineColorAndroid="#fff"
          />
        </View>
      </View>
    )
  };

  _loadButtonContainer = () => {
    return (
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.buttonOpacityStyle}>
          <Button style={{borderColor: '#cdcdcd', backgroundColor: '#03ac13'}} onPress={() => this._signin()}>
            <Text style={styles.buttonText}>เข้าสู่ระบบ</Text>
          </Button>
        </TouchableOpacity>

        <View style={{marginBottom: 15}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.onScreenText}>ยังไม่มีสมาชิก?</Text>
            <TouchableOpacity onPress={() => this._signup()}>
              <Text style={styles.signupText}> สมัครเลย </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  };


  render() {
    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior="padding">
        <StatusBar
          barStyle="default"
        />
        {this._loadFormContainer()}
        {this._loadButtonContainer()}
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  logoContainer: {
    alignItems: 'center',
    backgroundColor: '#000'
  },
  logoStyle: {
    height: height * .2,
  },
  formContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInputStyle: {
    height: height * 0.07,
    width: width * 0.90,
    borderColor: '#9A9A9A',
    borderWidth: 1,
    borderRadius: 4,
    paddingLeft: 15,
    color: '#616161',
    fontFamily: 'thai-sans',
    fontSize: 18,
  },
  buttonContainer: {
    alignItems: 'center',
  },
  buttonOpacityStyle: {
    width: '85%',
    marginTop: 5
  },
  buttonText: {
    fontSize: 20,
    color: '#fff',
    fontFamily: 'thai-sans-bold',
  },
  onScreenText: {
    color: '#616161',
    fontSize: 20,
    fontWeight: '300',
    fontFamily: 'thai-sans',
  },
  signupText: {
    color: '#616161',
    fontSize: 20,
    fontWeight: '300',
    textDecorationLine: 'underline',
    fontFamily: 'thai-sans',
  }
});
