import React from 'react'
import {
  StyleSheet,
  Text,
  ScrollView,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  StatusBar, Alert,
} from 'react-native'
import {
  MaterialIcons
} from '@expo/vector-icons'
import { Button, Icon } from 'react-native-elements'
import RadioForm from 'react-native-simple-radio-button'
import { getPackageProduct } from '../../api/product'
import _ from 'lodash'
import {checkSignin} from "../../api/auth";

const { width, height } = Dimensions.get('window')

export default class PackagePlanScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        cart: [],
        totalPrice: 0,
        duration: 1,
        deliverWeek: 1,
        products: [],
    }
  }

  static navigationOptions = ({navigation}) =>({
    headerMode: 'screen',
    headerTitle: 'แพ็คเกจข้าว',
    headerLeft: (
      <TouchableOpacity style={{marginLeft: 5}} onPress={() => navigation.pop()}>
        <MaterialIcons name={'chevron-left'} size={34} color={'#fff'} />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: '#03ac13',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      color: '#fff',
      fontFamily: 'thai-sans-bold',
      fontSize: 22,
    }
  })

  componentWillMount = async () => {
    const isSignin = await checkSignin()
    if (!isSignin) {
      Alert.alert(
          'ยังไม่ได้เข้าสู่ระบบ',
          'กรุณาเข้าสู่ระบบเพื่อทำการซื้อสินค้า',
          [
            {text: 'ตกลง', onPress: () => this.props.navigation.navigate('Signin')},
          ],
          {cancelable: false},
      )
    }
    await this._getProduct()
  }

  _getProduct = async () => {
    const products = await getPackageProduct()
    await this.setState({products})
  }

  _numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  

  _loadPackage = () => {
    return (
        <Image 
            style={{width: width, height: height*0.25, marginVertical: 5}}
            resizeMode={'contain'}
            source={require('../../assets/imgs/package.png')} />
    )
  }

  _calculateTotalPrice = async () => {
    let totalPrice = 0
    this.state.cart.forEach((item) => {
        totalPrice += parseFloat(item.price) * parseFloat(item.amount)
    })
    await this.setState({totalPrice})
  }

  _reduceItem = async (item) => {
    const index = _.findIndex(this.state.cart, ({_id}) => _id === item._id)
    if (index >= 0) {
      const cart = this.state.cart
      if (cart[index].amount <= 1) {
        cart.splice(index, 1)
      }
      else {
        cart[index].amount -= 1
      }
      await this.setState({cart})
    }
    await this._calculateTotalPrice()
    this.forceUpdate()
  }

  _addItem = async (item) => {
    const index = _.findIndex(this.state.cart, ({_id}) => _id === item._id)
    if (index < 0) {
      const cart = [...this.state.cart, {...item, amount: 1}]
      await this.setState({cart})
    }
    else {
      let cart = this.state.cart
      cart[index].amount += 1
      await this.setState({cart})
    }
    await this._calculateTotalPrice()
    this.forceUpdate()
  }

  _mapAmountInCart = (item) => {
    const index = _.findIndex(this.state.cart, ({_id}) => _id === item._id)
    return index < 0 ? 0 : this.state.cart[index].amount
  }

  _renderPackagePlan = () => {
    return (
        this.state.products.map((item, index) => {
            return (
                <View key={item._id} style={styles.cardContainer}>
                    <Image style={[styles.image, {flex: 2}]} source={{ uri: item.image }} resizeMode='contain' />
                    <View style={[styles.cardInner, {flex: 4}]}>
                        <Text style={styles.title}>{item.name}</Text>
                        <Text style={styles.subtitle}>{item.size}</Text>
                        <Text style={styles.price}>{this._numberWithCommas(item.price)} บาท</Text>
                    </View>
                    <View style={[styles.amountView, {flex: 3, marginRight: 5}]}>
                        <TouchableOpacity onPress={() => this._reduceItem(item)}><Icon name='remove-circle' size={30} /></TouchableOpacity>
                        <Text style={styles.amount}>{ this._mapAmountInCart(item) }</Text>
                        <TouchableOpacity onPress={() => this._addItem(item)}><Icon name='add-circle' size={30} /></TouchableOpacity>
                  </View>
                </View>
            )
        })
    )
  }

  _renderPackageDuration = () => {
    const radio_props = [
        {label: '1 เดือน', value: 1 },
        {label: '3 เดือน', value: 3 },
        {label: '6 เดือน', value: 6 },
        {label: '12 เดือน', value: 12 },
      ];

      return (
        <View style={{marginTop: 15, margin: 10}}>
            <Text style={[styles.title, {marginBottom: 10}]}>ระยะเวลาในการจัดส่ง</Text>
            <RadioForm
            style={{
              marginLeft: 10,
            }}
            labelStyle={{
              fontFamily: 'thai-sans',
              fontSize: 18,
            }}
            radio_props={radio_props}
            selectedButtonColor={'#8ABB35'}
            buttonColor={'#8ABB35'}
            labelColor={'#4A4A4A'}
            initial={0}
            onPress={(value) => {this.setState({duration: value})}}
            />
        </View>
      )
  }

  _chooseDay = () => {
    const radio_props = [
        {label: 'สัปดาห์ที่ 1 ของเดือน', value: 1 },
        {label: 'สัปดาห์ที่ 2 ของเดือน', value: 2 },
        {label: 'สัปดาห์ที่ 3 ของเดือน', value: 3 },
        {label: 'สัปดาห์ที่ 4 ของเดือน', value: 4 },
      ];

      return (
        <View style={{marginTop: 15, margin: 10}}>
            <Text style={[styles.title, {marginBottom: 2}]}>วันที่จะทำการจัดส่ง</Text>
            <Text style={[styles.subtitle, {marginBottom: 10}]}>ทางบริษัทจะทำการนัดหมายวันที่ลูกค้าสะดวกก่อนส่งสินค้าให้แก่ลูกค้า</Text>
            <RadioForm
            style={{
              marginLeft: 10,
            }}
            labelStyle={{
              fontFamily: 'thai-sans',
              fontSize: 18,
            }}
            radio_props={radio_props}
            selectedButtonColor={'#8ABB35'}
            buttonColor={'#8ABB35'}
            labelColor={'#4A4A4A'}
            initial={0}
            onPress={(value) => {this.setState({deliverWeek: value})}}
            />
        </View>
      )
  }

  _loadScrollView = () => {
    return (
      <ScrollView style={styles.scrollViewContainer}>
        { this._loadPackage() }
        { this._renderPackagePlan() }
        { this._renderPackageDuration() }
        { this._chooseDay() }
      </ScrollView>
    )
  }

  _loadButton = () => {
    return (
      <View style={styles.footerView}>
        <View style={styles.bill}>
          <View style={styles.bill_1}>
            <Text style={styles.priceText}>รวม</Text>
          </View>
          <View style={styles.bill_2}>
            <Text style={[styles.priceText]}>{ this._numberWithCommas(this.state.totalPrice) }      บาท</Text>
          </View>
        </View>
        <Button
          buttonStyle={[styles.button, {backgroundColor: '#8ABB35'}]}
          disabled={this.state.totalPrice === 0}
          onPress={() => {
            this.props.navigation.navigate('Payment', 
              {
                totalPrice: this.state.totalPrice,
                isPackage: true,
                cart: this.state.cart,
                duration: this.state.duration,
                deliverWeek: this.state.deliverWeek,
              })}
          }
          title='ชำระเงิน'
          fontFamily={'thai-sans-bold'}
          fontSize={24}
        />
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
        />
        { this._loadScrollView() }
        { this._loadButton() }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  scrollViewContainer: {
    flex: 1,
    backgroundColor: '#fbfbfb',
    width: width,
    marginBottom: height *0.2,
  },
  cardContainer: {
    flexDirection: 'row',
    flex: 1, 
    marginLeft: 4,
    margin: 2,
    borderWidth: 1,
    borderRadius: 5,
    alignItems: 'center',
    borderColor: '#ADADAD',
  },
  cardInner: {
    marginTop: 5,
    marginLeft: 15,
  },
  image: {
    flex: 1,
    height: width*0.27,
    width: width*0.27,
  },
  title: {
    fontSize: 20,
    color: '#454545',
    marginBottom: 1,
    fontFamily: 'thai-sans-bold',
  },
  subtitle: {
    fontSize: 18,
    color: '#656565',
    marginBottom: 7,
    fontFamily: 'thai-sans',
  },
  price: {
    fontSize: 26,
    fontWeight: '600',
    color: '#63902a',
    marginBottom: 10,
    fontFamily: 'thai-sans-bold',
  },
  amountView: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  amount: {
    margin: 15,
  },
  footerView: {
    alignItems: 'center',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
  },
  bill: {
    flexDirection: 'row',
    backgroundColor: '#03ac13',
    width: width,
    height: height*0.12,
    alignItems: 'center',
  },
  bill_1: {
    flex: 1,
    marginLeft: width*0.1,
  },
  bill_2: {
    flex: 1,
    alignItems: 'flex-end',
    marginRight: width*0.1,
  },
  priceText: {
    fontSize: 24,
    color: '#fff',
    fontFamily: 'thai-sans-bold',
  },
  button: {
    width: width,
    height: height*0.08
  }
})
