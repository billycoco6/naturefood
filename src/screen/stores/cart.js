import React from 'react'
import {
  StyleSheet,
  Text,
  ScrollView,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  StatusBar,
  Alert,
} from 'react-native'
import { Card, Button, Icon } from 'react-native-elements'
import {checkSignin} from "../../api/auth"

const { width, height } = Dimensions.get('window')

export default class CartScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      cart: [],
      totalPrice: 0,
    }
  }

  static navigationOptions = ({navigation}) =>({
    headerMode: 'screen',
    headerTitle: 'ตะกร้าสินค้า',
    headerStyle: {
      backgroundColor: '#03ac13',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      color: '#fff',
      fontFamily: 'thai-sans-bold',
      fontSize: 22
    },
    headerBackTitleStyle: {
      color: '#fff',
      fontFamily: 'thai-sans-bold',
      fontSize: 22
    },
  })

  componentWillMount = async () => {
    const isSignin = await checkSignin()
    if (!isSignin) {
      Alert.alert(
        'ยังไม่ได้เข้าสู่ระบบ',
        'กรุณาเข้าสู่ระบบเพื่อทำการซื้อสินค้า',
        [
          {text: 'ตกลง', onPress: () => this.props.navigation.navigate('Signin')},
        ],
        {cancelable: false},
      )
    }
    const cart = this.props.navigation.getParam('cart')
    await this.setState({ cart })
    await this._calculateTotalPrice()
  }

  _numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  _calculateTotalPrice = async () => {
    let totalPrice = 0
    await this.state.cart.forEach((item) => {
      totalPrice += item.price * parseFloat(item.amount)
    })
    await this.setState({totalPrice})
  }

  _reduceItem = async (_id) => {
    await this.state.cart.forEach((item) => {
      if (item._id == _id && item.amount > 0) {
        item.amount -= 1
      }
    })
    console.log(this.state.cart)
    await this._calculateTotalPrice()
    this.forceUpdate()
  }

  _addItem = async (_id) => {
    await this.state.cart.forEach((item) => {
      if (item._id == _id) {
        item.amount += 1
      }
    })
    console.log(this.state.cart)
    await this._calculateTotalPrice()
    this.forceUpdate()
  }

  _loadScrollView = () => {
    return (
      <ScrollView style={styles.scrollViewContainer}>
        {
          this.state.cart.map((item) => {
            return (
              <Card key={item._id} containerStyle={styles.card}>
                <View style={styles.internalCardView}>
                  <Image source={{uri: item.image}} resizeMode={'contain'} style={styles.image} />
                  <View style={styles.textView}>
                    <Text numberOfLines={1} style={styles.text}>{ item.name }</Text>
                    <Text style={styles.price}>{ this._numberWithCommas(item.price) } บาท</Text>
                  </View>
                  <View style={styles.amountView}>
                    <TouchableOpacity onPress={() => this._reduceItem(item._id)}><Icon name='remove-circle' size={30} /></TouchableOpacity>
                    <Text style={styles.amount}>{ item.amount }</Text>
                    <TouchableOpacity onPress={() => this._addItem(item._id)}><Icon name='add-circle' size={30} /></TouchableOpacity>
                  </View>
                </View>
              </Card>
            )
          })
        }
      </ScrollView>
    )
  }

  _loadButton = () => {
    return (
      <View style={styles.footerView}>
        <View style={styles.bill}>
          <View style={styles.bill_1}>
            <Text style={styles.priceText}>รวม</Text>
          </View>
          <View style={styles.bill_2}>
            <Text style={styles.priceText}>{ this._numberWithCommas(this.state.totalPrice) }      บาท</Text>
          </View>
        </View>
        <Button
          buttonStyle={{width: width, height: height*0.08, backgroundColor: '#8ABB35'}}
          disabled={this.state.totalPrice === 0}
          onPress={() => this.props.navigation.push('Payment', {totalPrice: this.state.totalPrice, cart: this.state.cart, isPackage: false})}
          title='ชำระเงิน'
          fontFamily={'thai-sans-bold'}
          fontSize={24}
        />
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
        />
        { this._loadScrollView() }
        { this._loadButton() }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#efefef',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  scrollViewContainer: {
    // flex: 1,
    backgroundColor: '#efefef',
    width: width,
    marginBottom: height*0.22,
  },
  card: {
    flex: 1,
    margin: 10,
    borderRadius: 5,
  },
  internalCardView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    flex: 1,
    width: width*0.2,
    height: width*0.2,
  },
  textView: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginLeft: width*0.1,
  },
  text: {
    fontSize: 20,
    fontFamily: 'thai-sans-bold',
    paddingRight: 5,
  },
  price: {
    fontSize: 18,
    fontFamily: 'thai-sans',
    color: '#656565',
    marginTop: 4,
  },
  amountView: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  amount: {
    margin: 15,
  },
  footerView: {
    alignItems: 'center',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
  },
  bill: {
    flexDirection: 'row',
    backgroundColor: '#03ac13',
    width: width,
    height: height*0.12,
    alignItems: 'center',
  },
  bill_1: {
    flex: 1,
    marginLeft: width*0.1,
  },
  bill_2: {
    flex: 1,
    alignItems: 'flex-end',
    marginRight: width*0.1,
  },
  priceText: {
    fontSize: 24,
    color: '#fff',
    fontFamily: 'thai-sans-bold',
  },
})
