import React from 'react'
import {
    ActivityIndicator,
  StyleSheet,
  Text,
  ScrollView,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
    StatusBar,
	Alert,
} from 'react-native'
import { Button } from 'react-native-elements'
import { getProfile } from '../../api/user'
import { createOrder } from '../../api/order'
import { createSubscription } from '../../api/subscription'

const { width, height } = Dimensions.get('window')

export default class CreditCardScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        disable: false,
        credit: 0,
        enoughCredit: false,
    }
  }

  static navigationOptions = ({navigation}) =>({
    headerMode: 'screen',
    headerTitle: 'My Wallet',
    headerStyle: {
      backgroundColor: '#03ac13',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      color: '#fff',
      fontFamily: 'thai-sans-bold',
      fontSize: 22
    },
    headerBackTitleStyle: {
      color: '#fff',
      fontFamily: 'thai-sans-bold',
      fontSize: 22
    },
    })

    componentDidMount = async () => {
        const profile = await getProfile()
        console.log(profile)
        const credit = profile.wallet
        this.setState({credit})
        console.log(this.props.navigation.getParam('totalPrice'))
        console.log(this.state.credit - this.props.navigation.getParam('totalPrice') >= 0)
        if (this.state.credit - this.props.navigation.getParam('totalPrice') >= 0) {
            await this.setState({enoughCredit: true})
        }
    }
    
    _title = () => {
        return (
            <View style={{alignItems: 'center'}}>
                <Text style={[styles.thText, {fontSize: 36}]}>สรุปเครดิตจากการซื้อ</Text>
            </View>
        )
    }

    _summary = () => {
        return (
            <View>
                <View style={{width: width*0.85, marginTop: 20, padding: 15, backgroundColor: '#fff', borderRadius: 10}}>
                    <Text style={[styles.thText, {fontSize: 28}]}>เครดิตใน My Wallet</Text>
                    <Text style={[styles.thText, {fontSize: 28, color: '#63902a'}]}>{this.state.credit} บาท</Text>
                </View>
                <View style={{width: width*0.85, marginTop: 20, padding: 15, backgroundColor: '#fff', borderRadius: 10}}>
                    <Text style={[styles.thText, {fontSize: 28}]}>เครดิตที่ต้องใช้</Text>
                    <Text style={[styles.thText, {fontSize: 28, color: '#63902a'}]}>{this.props.navigation.getParam('totalPrice')} บาท</Text>
                </View>
                <View style={{width: width*0.85, marginTop: 20, padding: 15, backgroundColor: '#8ABB35', borderRadius: 10}}>
                    <Text style={[styles.thText, {fontSize: 28, color: '#fff'}]}>คงเหลือ</Text>
                    <Text style={[styles.thText, {fontSize: 28, color: '#fff'}]}>{this.state.credit - this.props.navigation.getParam('totalPrice')} บาท</Text>
                </View>
            </View>
        )
    }

    _checkout = async () => {
        this.setState({disable: true})
        const cart = this.props.navigation.getParam('cart') 
		if (this.props.navigation.getParam('isPackage')) {
			console.log('This is package')
            // create subscription function
            this._createSubscription({cart})
		} else {
			console.log('This is not a package')
            this._createOrder({cart})
		}
    }

    _createSubscription = async ({cart}) => {
        const deliverWeek = this.props.navigation.getParam('deliverWeek')
        const duration = this.props.navigation.getParam('duration')
        const res = await createSubscription({
            cart,
            deliverWeek,
            duration,
        })
        console.log('Done create subscription')
        console.log({res})
        if (res.status === 200) {
            Alert.alert(
                'ชำระเงินสำเร็จ',
                'ออเดอร์ได้ถูกสร้างแล้ว',
                [
                    {text: 'ยืนยัน', onPress: () => this.props.navigation.navigate('SubscriptionHistory')}
                ],
            )
        } else if (res.status > 200) {
            Alert.alert(
                'มีข้อผิดพลาด',
                'กรุณาตรวจสอบจำนวน Credit ใน My Wallet',
                [
                    {text: 'ยืนยัน', onPress: () => console.log('OK Pressed')}
                ]
            )
            this.setState({disable: false})
        }
    }

    _createOrder = async ({cart}) => {
        const res = await createOrder({
            cart,
        })
        console.log('Done Charging')
        console.log({res})
        if (res.status === 200) {
            Alert.alert(
                'ชำระเงินสำเร็จ',
                'ออเดอร์ได้ถูกสร้างแล้ว',
                [
                    {text: 'ยืนยัน', onPress: () => this.props.navigation.navigate('History')}
                ],
            )
        } else if (res.status > 200) {
            Alert.alert(
                'มีข้อผิดพลาด',
                'กรุณาตรวจสอบจำนวน Credit ใน My Wallet',
                [
                    {text: 'ยืนยัน', onPress: () => console.log('OK Pressed')}
                ]
            )
        }
    }
	
	_submitButton = () => {
        let disable = false
        if (!this.state.enoughCredit || this.state.disable) {
            disable = true
        }
		return (
			<View style={{flex: 1, justifyContent: 'flex-end'}}>
				<Button
                    buttonStyle={{width: width, height: height*0.08, backgroundColor: '#8ABB35'}}
                    disabled={disable}
                    onPress={() => this._checkout()}
                    title='ยืนยันการชำระเงิน'
                    fontFamily={'thai-sans-bold'}
                    fontSize={24} />
			</View>
		)
	}

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
        />
        <View style={{margin: 20}}>
            { this._title() }
            { this._summary() }
        </View>
        <ActivityIndicator style={{marginBottom: 20}} animating={this.state.disable} size="large" color="#4A4A4A" />
        { this._submitButton() }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
        flex: 1,
        backgroundColor: '#efefef',
        alignItems: 'center',
		justifyContent: 'center',
	},
	button: {
		height: height*0.08,
		width: width,
		alignItems: 'center',
		justifyContent: 'center',
    },
    thText: {
        fontFamily: 'thai-sans',
    },
	buttonText: {
		fontSize: 24,
		fontFamily: 'thai-sans-bold',
		color: '#fff',
	},
})
