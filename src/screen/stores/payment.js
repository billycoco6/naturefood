import React from 'react'
import {
  StyleSheet,
  Text,
  ScrollView,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  StatusBar,
} from 'react-native'
import {
  MaterialIcons
} from '@expo/vector-icons'
import { Card, Button, Icon } from 'react-native-elements'

const { width, height } = Dimensions.get('window')

export default class PaymentScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      totalPrice: 0,
    }
  }

  static navigationOptions = ({navigation}) =>({
    headerMode: 'screen',
    headerTitle: 'ชำระเงิน',
    headerStyle: {
      backgroundColor: '#03ac13',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      color: '#fff'
    },
    headerTitleStyle: {
      color: '#fff',
      fontFamily: 'thai-sans-bold',
      fontSize: 22
    },
    headerBackTitleStyle: {
      color: '#fff',
      fontFamily: 'thai-sans-bold',
      fontSize: 22
    },
  })

  componentWillMount = async () => {
    const cart = this.props.navigation.getParam('cart')
    console.log('TOTAL PRICE:', this.props.navigation.getParam('totalPrice'))
    await this.setState({ cart })
  }

  _loadHeader = () => {
    return (
      <View style={styles.headerView}>
        <Text style={styles.headerText}>เลือกช่องทางการชำระเงิน</Text>
      </View>
    )
  }

  _loadOptions = () => {
    return (
      <ScrollView>
        <TouchableOpacity style={styles.optionView} onPress={() => this.props.navigation.navigate('CreditCard', 
          {
            totalPrice: this.props.navigation.getParam('totalPrice'),
            cart: this.props.navigation.getParam('cart'),
            isPackage: this.props.navigation.getParam('isPackage'),
            duration: this.props.navigation.getParam('duration'),
            deliverWeek: this.props.navigation.getParam('deliverWeek'),
          })}>
          <MaterialIcons style={styles.optionIcon} name='credit-card' size={36} color='#63902a' />
          <Text style={styles.optionName}>บัตรเครดิต</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.optionView} onPress={() => this.props.navigation.navigate('MyWalletCharge', 
          {
            totalPrice: this.props.navigation.getParam('totalPrice'),
            cart: this.props.navigation.getParam('cart'),
            isPackage: this.props.navigation.getParam('isPackage'),
            duration: this.props.navigation.getParam('duration'),
            deliverWeek: this.props.navigation.getParam('deliverWeek'),
          })}>
          <MaterialIcons style={styles.optionIcon} name='account-balance-wallet' size={36} color='#63902a' />
          <Text style={styles.optionName}>เครดิตใน My Wallet</Text>
        </TouchableOpacity>
      </ScrollView>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
        />
        { this._loadHeader() }
        { this._loadOptions() }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
  },
  headerView: {
    height: height*0.08,
    width: width,
    backgroundColor: '#eaeaea',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 20,
    fontFamily: 'thai-sans',
    color: '#818181',
  },
  optionView: {
    marginLeft: width*0.05,
    width: width*0.9,
    flexDirection: 'row',
    padding: width*0.07,
    borderBottomWidth: 1,
    borderColor: '#777777'
  },
  optionIcon: {
    marginLeft: width*0.01,
  },
  optionName: {
    marginLeft: width*0.1,
    marginTop: 5,
    fontSize: 22,
    fontFamily: 'thai-sans-bold',
    color: '#656565',
  },
})
