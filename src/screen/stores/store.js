import React from 'react'
import {
  StyleSheet,
  Text,
  ScrollView,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  StatusBar,
  AsyncStorage,
} from 'react-native'
import {
  MaterialIcons
} from '@expo/vector-icons'
import {Card, Button, SearchBar} from 'react-native-elements'
import IconBadge from 'react-native-icon-badge'
import {checkSignin} from '../../api/auth'
import {getProduct} from '../../api/product'
import {NavigationEvents} from "react-navigation"

const {width, height} = Dimensions.get('window')

export default class StoreScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      checkSignin: false,
      cart: [],
      products: [],
    }
  }

  componentDidMount = async () => {
    await this._updateCart()
    await this._getProduct()
  }

  _getProduct = async () => {
    const products = await getProduct()
    this.setState({products})
  }

  _numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
  }

  _updateCart = async () => {
    let totalAmount = await this._cartTotalAmount()
    this.props.navigation.setParams({cart: this.state.cart, totalAmount: totalAmount})
  }

  static navigationOptions = ({navigation}) => {
    let isSignin = false
    let {params} = navigation.state
    if (params !== undefined && params.hasOwnProperty('isSignin')) {
      console.log({params})
      isSignin = params.isSignin
      console.log('isSignin', isSignin)
    }
    return {
      headerMode: 'screen',
      headerTitle: 'ร้านค้า',
      headerLeft: (
        isSignin ?
        <TouchableOpacity style={{marginLeft: 5}} onPress={() => navigation.openDrawer()}>
          <MaterialIcons name={'list'} size={30} color={'#fff'}/>
        </TouchableOpacity> :
        <View/>
      ),
      headerRight: (
        <IconBadge
          MainElement={
            <TouchableOpacity style={{marginRight: 10}} onPress={() => {
              navigation.push('Cart', {
                cart: navigation.getParam('cart'),
              })
            }}>
              <MaterialIcons name={'shopping-cart'} size={25} color={'#fff'}/>
            </TouchableOpacity>
          }
          BadgeElement={
            <TouchableOpacity onPress={() => {
              navigation.push('Cart', {
                cart: navigation.getParam('cart'),
              })
            }}>
              <Text style={{color: '#FFFFFF'}}>{navigation.getParam('totalAmount')}</Text>
            </TouchableOpacity>
          }
          IconBadgeStyle={
            {
              width: 20,
              height: 20,
              backgroundColor: '#d02d2e',
              top: -7,
              right: 4,
            }
          }
          Hidden={navigation.getParam('totalAmount') == 0}
        />
      ),
      headerStyle: {
        backgroundColor: '#03ac13',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        color: '#fff',
        fontFamily: 'thai-sans-bold',
        fontSize: 22,
      }
    }
  }

  _cartTotalAmount = async () => {
    let totalAmount = 0
    await this.state.cart.forEach((prod) => {
      totalAmount += prod.amount
    })
    return totalAmount
  }

  _loadSearchBar = () => {
    return (
      <View style={styles.searchbarContainer}>
      </View>
    )
  }

  _loadPackage = () => {
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('PackagePlan')}>
        <Image
          style={{width: width, height: height * 0.25, marginVertical: 5}}
          resizeMode={'contain'}
          source={require('../../assets/imgs/package.png')}/>
      </TouchableOpacity>
    )
  }

  _addToCardButton = (item) => {
    return (
      <Button
        onPress={async () => {
          let isin = false
          this.state.cart.forEach((prod) => {
            if (prod._id === item._id) {
              prod.amount += 1
              isin = true
            }
          })
          if (!isin) {
            let prod = item
            prod['amount'] = 1
            let cart = this.state.cart.concat(prod)
            await this.setState({cart})
          }
          let totalAmount = await this._cartTotalAmount()
          this.props.navigation.setParams({cart: this.state.cart, totalAmount: totalAmount})
          console.log(this.state.cart)
        }}
        small
        style={{width: width * 0.48}}
        backgroundColor={'#03ac13'}
        fontFamily={'thai-sans-bold'}
        fontSize={20}
        title='เพิ่มลงตะกร้า'/>
    )
  }

  _renderPackagePlan = () => {
    if (this.state.products.length % 2 === 0) { // when products length is even
      return (
        this.state.products.map((item, index) => {
          if ((index + 1) % 2 === 0) {
            return (
              <View key={this.state.products[index - 1]._id} style={styles.cardContainer}>
                <View style={styles.cardLeft}>
                  <Image style={styles.image} source={{uri: this.state.products[index - 1].image}}
                         resizeMode='contain'/>
                  <View style={styles.cardInner}>
                    <Text style={styles.title}>{this.state.products[index - 1].name}</Text>
                    <Text style={styles.subtitile}>{this.state.products[index - 1].size}</Text>
                    <Text style={styles.price}>{this._numberWithCommas(this.state.products[index - 1].price)} บาท</Text>
                  </View>
                  {this._addToCardButton(this.state.products[index - 1])}
                </View>
                <View style={[styles.cardRight, {borderWidth: 1}]}>
                  <Image style={styles.image} source={{uri: this.state.products[index].image}} resizeMode='contain'/>
                  <View style={styles.cardInner}>
                    <Text style={styles.title}>{this.state.products[index].name}</Text>
                    <Text style={styles.subtitile}>{this.state.products[index].size}</Text>
                    <Text style={styles.price}>{this._numberWithCommas(this.state.products[index].price)} บาท</Text>
                  </View>
                  {this._addToCardButton(this.state.products[index])}
                </View>
              </View>
            )
          }
        })
      )
    } else { // when products length is odd
      return (
        this.state.products.map((item, index) => {
          if ((index + 1) % 2 === 0) {
            return (
              <View key={this.state.products[index - 1]._id} style={styles.cardContainer}>
                <View style={styles.cardLeft}>
                  <Image style={styles.image} source={{uri: this.state.products[index - 1].image}}
                         resizeMode='contain'/>
                  <View style={styles.cardInner}>
                    <Text style={styles.title}>{this.state.products[index - 1].name}</Text>
                    <Text style={styles.subtitile}>{this.state.products[index - 1].size}</Text>
                    <Text style={styles.price}>{this._numberWithCommas(this.state.products[index - 1].price)} บาท</Text>
                  </View>
                  {this._addToCardButton(this.state.products[index - 1])}
                </View>
                <View style={[styles.cardRight, {borderWidth: 1}]}>
                  <Image style={styles.image} source={{uri: this.state.products[index].image}} resizeMode='contain'/>
                  <View style={styles.cardInner}>
                    <Text style={styles.title}>{this.state.products[index].name}</Text>
                    <Text style={styles.subtitile}>{this.state.products[index].size}</Text>
                    <Text style={styles.price}>{this._numberWithCommas(this.state.products[index].price)} บาท</Text>
                  </View>
                  {this._addToCardButton(this.state.products[index])}
                </View>
              </View>
            )
          } else if (index === this.state.products.length - 1) {
            return (
              <View key={this.state.products[index]._id} style={styles.cardContainer}>
                <View style={styles.cardLeft}>
                  <Image style={styles.image} source={{uri: this.state.products[index].image}} resizeMode='contain'/>
                  <View style={styles.cardInner}>
                    <Text style={styles.title}>{this.state.products[index].name}</Text>
                    <Text style={styles.subtitile}>{this.state.products[index].size}</Text>
                    <Text style={styles.price}>{this._numberWithCommas(this.state.products[index].price)} บาท</Text>
                  </View>
                  {this._addToCardButton(this.state.products[index])}
                </View>
                <View style={styles.cardRight}></View>
              </View>
            )
          }
        })
      )
    }
  }

  _loadScrollView = () => {
    return (
      <ScrollView style={styles.scrollViewContainer}>
        {this._loadPackage()}
        {this._renderPackagePlan()}
      </ScrollView>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
        />
        <NavigationEvents
          onWillFocus={async () => {
            if (!this.state.checkSignin) {
              const isSignin = await checkSignin()
              console.log('set params', isSignin)
              this.props.navigation.setParams({'isSignin': isSignin})
              this.setState({checkSignin: true})
            }
          }}
        />
        {this._loadScrollView()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  searchbarContainer: {
    width: width
  },
  searchbar: {
    backgroundColor: '#03ac13'
  },
  scrollViewContainer: {
    flex: 1,
    backgroundColor: '#fbfbfb',
    width: width,
  },
  cardContainer: {
    flexDirection: 'row',
  },
  cardLeft: {
    flex: 1,
    marginLeft: 4,
    margin: 2,
    borderWidth: 1,
    alignItems: 'center',
    borderColor: '#ADADAD',
  },
  cardRight: {
    flex: 1,
    marginRight: 4,
    margin: 2,
    alignItems: 'center',
    borderColor: '#ADADAD',
  },
  cardInner: {
    marginLeft: 5,
  },
  image: {
    flex: 1,
    height: width * 0.35,
    width: width * 0.35,
  },
  title: {
    fontSize: 20,
    marginTop: 5,
    color: '#454545',
    marginBottom: 1,
    fontFamily: 'thai-sans-bold',
  },
  subtitile: {
    fontSize: 18,
    fontFamily: 'thai-sans',
    color: '#656565',
    marginBottom: 7,
  },
  price: {
    fontSize: 26,
    fontFamily: 'thai-sans-bold',
    color: '#63902a',
    marginBottom: 10,
  },
})
