import React from 'react'
import {
	ActivityIndicator,
  StyleSheet,
  Text,
  ScrollView,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
	StatusBar,
	Alert,
} from 'react-native'
import {
  MaterialIcons
} from '@expo/vector-icons'
import {
	CreditCardInput
} from 'react-native-credit-card-input'
import { Button } from 'react-native-elements'
import { getCardToken } from '../../api/payment'
import { createOrder } from '../../api/order'
import { createSubscription } from '../../api/subscription'

const { width, height } = Dimensions.get('window')

export default class CreditCardScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
			disable: false,
			isValid: false,
			number: '',
			expiry: '',
			cvc: '',
			name: '',
    }
  }

  static navigationOptions = ({navigation}) =>({
    headerMode: 'screen',
    headerTitle: 'บัตรเครดิต',
    headerStyle: {
      backgroundColor: '#03ac13',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      color: '#fff',
      fontFamily: 'thai-sans-bold',
      fontSize: 22
    },
    headerBackTitleStyle: {
      color: '#fff',
      fontFamily: 'thai-sans-bold',
      fontSize: 22
    },
	})
	
	_onChange = (form) => {
		let isValid = false
		if (form.status.number === 'valid' && form.status.expiry === 'valid' && form.status.cvc === 'valid' && form.status.name === 'valid') {
			let number = form.values.number
			let expiry = form.values.expiry
			let cvc = form.values.cvc
			let name = form.values.name
			isValid = true
			this.setState({ 
				number,
				expiry,
				cvc,
				name,
				isValid,
			})
		} else {
			this.setState({ isValid })
		}
	}

  _cardInput = () => {
    return (
      <View style={{flex: 10, marginTop: height*0.05}}>
				<CreditCardInput 
					onChange={this._onChange}
					requiresName={true} />
      </View>
    )
	}

	// _creditCardCharge = async () => {
	// 	const res = await getCardToken({
	// 		cart: this.props.navigation.getParam('cart'),
	// 		totalPrice: this.props.navigation.getParam('totalPrice'),
	// 		isPackage: this.props.navigation.getParam('isPackage'),
	// 		duration: this.props.navigation.getParam('duration'),
	// 		deliverWeek: this.props.navigation.getParam('deliverWeek'),
	// 		number: this.state.number,
	// 		expirationMonth: this.state.expiry.substring(0,2),
	// 		expirationYear: this.state.expiry.substring(3,5),
	// 		securityCode: this.state.cvc,
	// 		name: this.state.name,
	// 	})
	// 	if (res.response.status === 200) {
	// 		Alert.alert(
	// 			'ชำระเงินสำเร็จ',
	// 			'ออเดอร์ได้ถูกสร้างแล้ว',
	// 			[
	// 				{text: 'ยืนยัน', onPress: () => this.props.navigation.navigate('History')}
	// 			],
	// 		)
	// 	} else if (res.response.status > 200) {
	// 		Alert.alert(
	// 			'มีข้อผิดพลาด',
	// 			'กรุณาตรวจสอบข้อมูลของบัตร',
	// 			[
	// 				{text: 'ยืนยัน', onPress: () => console.log('OK Pressed')},
	// 			],
	// 		)			
	// 	}
	// }

	_checkout = async () => {
		this.setState({disable: true})
		const cardInfo = await getCardToken({
			cart: this.props.navigation.getParam('cart'),
			totalPrice: this.props.navigation.getParam('totalPrice'),
			isPackage: this.props.navigation.getParam('isPackage'),
			duration: this.props.navigation.getParam('duration'),
			deliverWeek: this.props.navigation.getParam('deliverWeek'),
			number: this.state.number,
			expirationMonth: this.state.expiry.substring(0,2),
			expirationYear: this.state.expiry.substring(3,5),
			securityCode: this.state.cvc,
			name: this.state.name,
		})
		if (this.props.navigation.getParam('isPackage')) {
			console.log('This is package')
			// create subscription function
			this._createSubscription({cardInfo})
		} else {
			console.log('This is not a package')
			this._createOrder({cardInfo})
		}
	}

	_createSubscription = async (cardInfo) => {
		const res = await createSubscription({
			cart: this.props.navigation.getParam('cart'),
			deliverWeek: this.props.navigation.getParam('deliverWeek'),
			duration: this.props.navigation.getParam('duration'),
			cardInfo,
		})
		console.log({res})
		if (res.status === 200) {
			Alert.alert(
					'ชำระเงินสำเร็จ',
					'ออเดอร์ได้ถูกสร้างแล้ว',
					[
							{text: 'ยืนยัน', onPress: () => this.props.navigation.navigate('SubscriptionHistory')}
					],
			)
		} else if (res.response.status > 200) {
			Alert.alert(
					'มีข้อผิดพลาด',
					'กรุณาตรวจสอบจำนวน Credit ใน My Wallet',
					[
							{text: 'ยืนยัน', onPress: () => this.setState({disable: false})}
					]
			)
		}
	}

	_createOrder = async (cardInfo) => {
		const res = await createOrder({
			cart: this.props.navigation.getParam('cart'),
			cardInfo,
		})
		console.log({res})
		if (res.status === 200) {
			Alert.alert(
					'ชำระเงินสำเร็จ',
					'ออเดอร์ได้ถูกสร้างแล้ว',
					[
							{text: 'ยืนยัน', onPress: () => this.props.navigation.navigate('History')}
					],
			)
		} else if (res.response.status > 200) {
			Alert.alert(
					'มีข้อผิดพลาด',
					'กรุณาตรวจสอบจำนวน Credit ใน My Wallet',
					[
							{text: 'ยืนยัน', onPress: () => this.setState({disable: false})}
					]
			)
		}
}
	
	_submitButton = () => {
		return (
			<View style={{flex: 1, justifyContent: 'flex-end'}}>
				<Button
          buttonStyle={{width: width, height: height*0.08, backgroundColor: '#8ABB35'}}
          disabled={this.state.disable}
          onPress={() => this._checkout()}
          title='ยืนยันการชำระเงิน'
          fontFamily={'thai-sans-bold'}
          fontSize={24}
        />
			</View>
		)
	}

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
        />
				{ this._cardInput() }
				<ActivityIndicator style={{marginBottom: 20}} animating={this.state.disable} size="large" color="#4A4A4A" />
				{ this._submitButton() }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#efefef',
		alignItems: 'center',
		justifyContent: 'center',
	},
	button: {
		height: height*0.08,
		width: width,
		alignItems: 'center',
		justifyContent: 'center',
	},
	buttonText: {
		fontSize: 24,
		fontFamily: 'thai-sans-bold',
		color: '#fff',
	},
})
