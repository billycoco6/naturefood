import Expo from 'expo'
import React from 'react'
import {
  StatusBar,
  StyleSheet,
  Text,
  Dimensions,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Alert,
  AsyncStorage,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native'
import Button from 'apsl-react-native-button'
import {
  CheckBox
} from 'react-native-elements'
import {isEmail} from 'validator'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import {signup, signin} from '../api/auth'

const {width, height} = Dimensions.get('window')

export default class RegisterScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: '',
      address: '',
      tel: '',
      showPassword: false
    };
  }

  static navigationOptions = {
    title: 'สมัครสมาชิก',
    headerStyle: {
      backgroundColor: '#03ac13',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      color: '#fff',
      fontFamily: 'thai-sans-bold',
      fontSize: 22
    },
    headerBackTitleStyle: {
      color: '#fff',
      fontFamily: 'thai-sans-bold',
      fontSize: 22
    },
  }

  _signup = async () => {
    try {
      if (!isEmail(this.state.email)) {
        throw new Error('กรุณาตรวจสอบ email')
      }
      if (this.state.password.length < 6) {
        throw new Error('Password (ขั้นต่ำ 6 ตัวอักษร)')
      }

      const res = await signup({
        email: this.state.email,
        password: this.state.password,
        name: this.state.name,
        address: this.state.address,
        tel: this.state.tel,
      })

      if (res.status === 201) {
        Alert.alert(
          'สร้างบัญชีสำเร็จ',
          '',
          [
            {
              text: 'OK', onPress: async () => {
                let res = await signin(
                  {
                    email: this.state.email,
                    password: this.state.password,
                  })
                await AsyncStorage.setItem('token', res.data)
                this.props.navigation.navigate('App')
              }
            }
          ]
        )
      }
    } catch (e) {
      const status = e.response.status
      switch (e.response.status) {
        case 400:
          Alert.alert(
            'ข้อมูลผิดพลาด',
            'กรุณาตรวจสอบ email และ password (ขั้นต่ำ 6 ตัวอักษร)',
            [
              {
                text: 'OK', onPress: async () => {
                }
              }
            ]
          )
          break
        case 409:
          Alert.alert(
            'ข้อมูลผิดพลาด',
            'มีบัญชีนี้แล้วในระบบ',
            [
              {
                text: 'OK', onPress: async () => {
                }
              }
            ]
          )
      }
    }
  }

  _getfbInfo = async () => {
    const {type, token} = await Expo.Facebook.logInWithReadPermissionsAsync('255577341939552',
      {
        permissions: ['public_profile', 'email'],
      })
    if (type === 'success') {
      // Get the user's name using Facebook's Graph API
      const response = await fetch(
        `https://graph.facebook.com/me?fields=id,name,email&access_token=${token}`)
      let fbres = await response.json()
      return this.setState({name: fbres.name, email: fbres.email})
    }
  }

  _loadFormContainer = () => {
    return (
      <View style={styles.formContainer}>
        <Image style={styles.logoStyle}
               resizeMode="contain"
               source={require('../assets/logo.png')}/>
        <View>
          <Text style={[styles.titleStyle, {fontSize: 24}]}>สมัครบัญชีผู้ใช้</Text>
          <Text style={[styles.titleStyle, {fontSize: 18, color: '#03ac13'}]}>เพื่อสะสมคะแนนใช้แทนเงินสดและรับสิทธิส่งข้าวฟรีถึงบ้าน</Text>
        </View>
        <TextInput style={styles.textInputStyle}
                   value={this.state.name}
                   onChangeText={(name) => this.setState({name})}
                   placeholder={"ชื่อ-นามสกุล"}
                   placeholderTextColor="#cdcdcd"
                   autoCapitalize="none"
                   autoCorrect={false}
                   underlineColorAndroid="#fff"
        />
        <TextInput style={styles.textInputStyle}
                   value={this.state.email}
                   onChangeText={(email) => this.setState({email})}
                   placeholder={"อีเมล์"}
                   placeholderTextColor="#cdcdcd"
                   autoCapitalize="none"
                   autoCorrect={false}
                   underlineColorAndroid="#fff"
        />
        <TextInput style={styles.textInputStyle}
                   value={this.state.password}
                   onChangeText={(password) => this.setState({password})}
                   placeholder={"รหัสผ่าน"}
                   secureTextEntry={!this.state.showPassword}
                   placeholderTextColor="#cdcdcd"
                   autoCapitalize="none"
                   autoCorrect={false}
                   underlineColorAndroid="#fff"
        />
        <TextInput style={[styles.textInputStyle, {height: height * 0.08}]}
                   value={this.state.address}
                   onChangeText={(address) => this.setState({address})}
                   placeholder={"ที่อยู่"}
                   placeholderTextColor="#cdcdcd"
                   autoCapitalize="none"
                   autoCorrect={false}
                   underlineColorAndroid="#fff"
        />
        <TextInput style={styles.textInputStyle}
                   value={this.state.tel}
                   onChangeText={(tel) => this.setState({tel})}
                   placeholder={"เบอร์โทรศัพท์"}
                   placeholderTextColor="#cdcdcd"
                   autoCapitalize="none"
                   autoCorrect={false}
                   underlineColorAndroid="#fff"
                   keyboardType={"numeric"}
        />
        <Text style={[styles.titleStyle, {fontSize: 20, color: '#616161'}]}>*รหัสผ่านต้องเป็นตัวเลขหรืออักษรอย่างน้อย 6 ตัว</Text>
        <CheckBox
          title='แสดงรหัสผ่าน'
          checked={this.state.showPassword}
          textStyle={[styles.titleStyle, {fontSize: 18, color: '#616161'}]}
          containerStyle={{backgroundColor: '#fff'}}
          onPress={() => this.setState({showPassword: !this.state.showPassword})}/>
      </View>
    )
  };

  _loadButtonContainer = () => {
    return (
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.buttonOpacityStyle}>
          <Button style={{borderColor: '#cdcdcd', backgroundColor: '#03ac13'}} onPress={() => this._signup()}>
            <Text style={styles.buttonText}>ยืนยันสร้างบัญชีผู้ใช้</Text>
          </Button>
          <Button style={{borderColor: '#cdcdcd', backgroundColor: '#03ac13'}} onPress={() => this._getfbInfo()}>
            <Text style={styles.buttonText}>Sign up with Facebook</Text>
          </Button>
        </TouchableOpacity>
      </View>
    )
  };


  render() {
    return (
      <KeyboardAwareScrollView
        innerRef={ref => {
          this.scroll = ref
        }}
        enableOnAndroid={true}
        style={styles.container}>
        <StatusBar
          barStyle="light-content"
        />
        {this._loadFormContainer()}
        {this._loadButtonContainer()}
      </KeyboardAwareScrollView
      >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
  },
  logoStyle: {
    marginTop: 15,
    height: height * .15,
  },
  titleStyle: {
    fontFamily: 'thai-sans',
  },
  formContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInputStyle: {
    height: height * 0.07,
    width: width * 0.90,
    borderColor: '#9A9A9A',
    borderWidth: 1,
    borderRadius: 4,
    paddingLeft: 15,
    color: '#616161',
    fontFamily: 'thai-sans',
    fontSize: 18,
    marginVertical: 2,
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonOpacityStyle: {
    width: '85%',
    marginTop: 5,
  },
  buttonText: {
    fontSize: 20,
    color: '#fff',
    fontFamily: 'thai-sans-bold',
  }
});
