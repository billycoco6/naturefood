import React from 'react'
import {
  AsyncStorage,
  StatusBar,
  StyleSheet,
  Text,
  Dimensions,
  View,
  TouchableOpacity,
  Image,
} from 'react-native'

const {width, height} = Dimensions.get('window')

export default class WelcomeScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = {header: null};

  componentDidMount = async () => {
    await AsyncStorage.removeItem('token')
  }

  _upperContainer = () => {
    return (
      <View style={styles.upperContainer}>


        <View style={{flex: 1, marginTop: height*0.02}}>
          <Image style={styles.logoStyle}
                 resizeMode="contain"
                 source={require('../assets/logo.png')}/>
        </View>


        <View style={styles.textContainer}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('App')}>
            <Text style={[styles.onScreenText]}>เข้าสู่ร้านค้า</Text>
          </TouchableOpacity>
        </View>

      </View>
    )
  };

  _lowerContainer = () => {
    return (
      <View style={styles.lowerContainer}>
        <Image style={styles.imageStyle}
               source={require('../assets/imgs/welcome.png')}/>
      </View>
    )
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="default"
        />
        {this._upperContainer()}
        {this._lowerContainer()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  upperContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  textContainer: {
    flexDirection: 'row',
    marginTop: height*0.02,
    marginRight: width*0.05,
  },
  lowerContainer: {
    flex: 9,
    alignItems: 'center'
  },
  logoStyle: {
    width: width * .3,
    marginLeft: width * .05
  },
  imageStyle: {
    height: height*.9,
    width: width
  },
  onScreenText: {
    color: '#4a4a4a',
    fontSize: 22,
    fontFamily: 'thai-sans'
  }
});
